#!/usr/bin/perl -w
use strict;

#### Copyright 2008, Devendra Gera <gera@theoldmonk.net>,
#### All rights reserved.
####
#### This is Free Software, released under the terms of the GNU General Public
#### License, version 2. A copy of the license can be obtained by emailing the
#### author, or from http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
####
#### As noted in the License, this software does not come with any warranty,
#### explicit or implied. This program might, and would be buggy. Use it at
#### your own risk.

use Pod::Usage;
use Term::ReadLine;
use Fcntl qw(:flock);

my $todo_file = "$ENV{HOME}/todo/todo.txt";
my $done_file = "$ENV{HOME}/todo/done.txt";
my $not_done_file = "$ENV{HOME}/todo/not_done.txt";

my %dispatch = (
	add		=> \&add_task,
	ls		=> \&list_tasks,
	do		=> \&do_task,
	contexts	=> \&list_contexts,
	groups		=> \&list_groups,
	lsall		=> \&listall,
	del		=> \&delete_task,
	change		=> \&change_task,
);
my @tasks;

my $command = shift || "ls";
pod2usage()
	unless ( (defined $command) && (exists $dispatch{$command}) );

read_tasks();
$dispatch{$command}->(@ARGV);

exit(0);

sub add_task
{
	my $task = join (" ", @_);
	quit("") unless $task;

	# find out the group of the task
	my (undef, $group) = ( $task =~ m{(^|\s)(/\S*)(\s|$)} );
	$group |= "";
	my ($parent, $number) = ( $group =~ /(.*?)\.(\d+)/ );

	my $target_offset = @tasks;

	# insert properly if its a subtask
	if(defined $parent) {

		# also remember to strip the number
		$task =~ s/$parent\.$number/$parent/;

		my @candidate_ids = grep_pos_in_tasks($parent, ());
		my $length = @candidate_ids;

		if( $number <= $length ) {
			$target_offset = $candidate_ids[$number - 1] - 1;
		}
	}

	splice @tasks, $target_offset, 0, ($task);

	write_tasks();
}

sub list_tasks
{
	my @patterns = @_;
	@patterns = ("") unless @patterns;
	my $is_a_group = 0;

	my @candidates = ();
	foreach my $pattern ( @patterns ) {
		$is_a_group = 1 if( $pattern =~ m{^/} );
		@candidates = grep_pos_in_tasks( $pattern, @candidates );
	}

	# list tasks only if they are the first task from their group.
	my %out;
	my $i = 0;
	foreach my $id ( @candidates ) {
		unless ($is_a_group) {
			my (undef,$group) = ( $tasks[$id - 1] =~ m{(^|\s)(/\S*)(\s|$)} );
			if(defined $group) {
				my $first = (grep_pos_in_tasks( $group, () ))[0];
				$out{$group} = $id if ($id == $first);
				next;
			}
		}
		$out{$i++} = $id;
	}

	print_tasks( values %out );
}

sub listall
{
	my @patterns = @_;
	return print_tasks( (1 .. scalar(@tasks)) )
		unless @patterns;

	my @candidates = ();
	foreach my $pattern ( @patterns ) {
		@candidates = grep_pos_in_tasks( $pattern, @candidates );
	}

	# list only one task per group from these results.
	my %out;
	my $i = 0;
	foreach my $id ( @candidates ) {
		my (undef,$group) = ( $tasks[$id - 1] =~ m{(^|\s)(/\S*)(\s|$)} );
		if(defined $group) {
			$out{$group} = $id if (!exists $out{$group});
			next;
		}
		$out{$i++} = $id;
	}

	print_tasks( values %out );
}

sub move_task
{
	my $num = shift;
	my $file = shift || quit("no file to move to!");

	quit("invalid task number") unless ((defined $num) && ($num =~ /^\d+$/));
	quit("") if ($num > @tasks);

	my $task = splice @tasks, $num - 1, 1;
	append_to_file($task, $file);
	write_tasks();
}

sub do_task
{
	my $flag = shift;
	my $num = ($flag =~ /^\d+$/) ? $flag : shift;

	my $ask = ($flag eq "-i") ? 1 : 0;
	if($ask) {
		print "delete task $num <$tasks[$num - 1]> ?";
		my $resp = <STDIN>;
		chomp $resp;
		return if ($resp =~ /^n/i);	# return if negative
	}

	move_task($num, $done_file);
}

sub delete_task
{
	my $num = shift;
	move_task($num, $not_done_file);
}

sub read_tasks
{
	open TODO, "<$todo_file" or return;

	while(<TODO>) {
		chomp;
		next unless $_;
		push @tasks, $_;
	}

	close TODO;
}

sub write_tasks
{
	open LOCK, ">$todo_file.lock" or quit("cannot open lock file : $!");
	flock LOCK, LOCK_EX;

	open TODO, ">$todo_file.$$" or quit("cannot open $todo_file.$$ : $!");
	print TODO join( "\n", @tasks );
	close TODO;

	rename( "$todo_file.$$", $todo_file ) or quit("cannot rename : $!");

	flock LOCK, LOCK_UN;
	close LOCK;
}

sub grep_pos_in_tasks
{
	my $pattern = shift;
	my @positions = @_;

	@positions = ( 1 .. scalar(@tasks) ) if( @positions < 1 );

	my @ret = ();
	foreach my $pos ( @positions ) {
		push @ret, $pos if ( $tasks[$pos - 1] =~ /$pattern/ );
	}

	return @ret;
}

sub append_to_file
{
	my $task = shift or quit("no task to append!");
	my $file = shift or quit("no file to append to!");
	my ($day, $mon, $year) = (localtime) [3 .. 5];
	$mon ++;
	$year += 1900;

	open LOCK, ">$file.lock" or quit("cannot open lock file : $!");
	flock LOCK, LOCK_EX;

	open DONE, ">>$file" or quit("cannot open $file.$$ : $!");
	print DONE "$year-$mon-$day : $task", "\n";
	close DONE;

	flock LOCK, LOCK_UN;
	close LOCK;
}

sub quit
{
	my $msg = shift;

	die $msg if $msg;
}

sub extract_tokens
{
	my $pattern = shift;
	my %ret;

	foreach ( @tasks ) {
		$ret{$2}++ if (/(^|\s)($pattern.*?)(\s|$)/);
	}

	return keys %ret;
}

sub list_contexts
{
	my @contexts = extract_tokens('@');
	print join( "\n", @contexts, "" );
}

sub list_groups
{
	my @groups = extract_tokens('/');
	print join( "\n", @groups, "" );
}

sub change_task
{
	my $num = shift;
	my $new_task = join(" ", @_);

	quit("not a valid task number") unless ((defined $num) && ($num =~ /^\d+$/));
	quit("") if ($num > @tasks);

	if($new_task =~ /^\s*$/) {
		# wow - we don't have a task here - so we'll get one.
		my $term = new Term::ReadLine 'gtdo';
		$new_task = $term->readline( "new task >", $tasks[$num - 1] );
	}

	return if($new_task =~ /^\s*$/);

	$tasks[$num - 1] = $new_task;
	write_tasks();
}

sub print_tasks
{
	my @ids = @_;

	foreach my $id ( sort { $a <=> $b } @ids ) {
		print "$id : $tasks[$id - 1]", "\n";
	}
}

